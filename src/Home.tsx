import React, { Component } from 'react';
import { IProviderProps } from './AutoAsasProps';
import homeHeader from './pictures/homeHeader.jpg';
import avatar from './pictures/avatar.png';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { inject, observer } from "mobx-react";
import ReviewCard from './components/ReviewCard';
import { NavLink } from 'react-router-dom';
import { Line, ChartData } from 'react-chartjs-2';
import {
    withGoogleMap,
    GoogleMap,
    Marker,
} from "react-google-maps";
import Navbar from './components/Navbar';
import './App.css';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';

@inject("store")
@observer
export default class Home extends Component<IProviderProps> {
    render() {
        let data = {
            //Bring in data
            labels: ["Saus.", "Vas.", "Kov.", "Bal.", "Gėg.", "Birž.", "Liep."],
            datasets: [
                {
                    label: "Klientai",
                    data: [0, 400, 800, 1500, 1600, 1700, 2000],
                    backgroundColor: "rgba(37, 155, 154, 0.7)"
                }
            ]
        }
        return (
            <div className="Home">
                <div className="HomeHeader" >
                    <div id="headerImage" style={{ backgroundImage: `url(${homeHeader})` }}></div>
                    <div id="headerCover" >
                        <div className="AutoAsasBest" >
                            <div id="title" >AutoAsas</div>
                            <div id="description" >autoservisų paieškos portalas <strong>Nr. 1</strong> Lietuvoje</div>
                        </div>
                        {
                            !this.props.store!.isRepairShop && this.props.store!.loggedIn &&
                            <NavLink to={"/MyBids?dialog=true"}>
                                <Fab id="createABid" variant="extended" aria-label="like">
                                    <AddIcon />
                                    Sukurti užsakymą
                                </Fab>
                            </NavLink>
                        }
                        {
                            !this.props.store!.isRepairShop && !this.props.store!.loggedIn &&
                            <Fab onClick={() => { this.props.store!.openLoginDialog = true }} id="createABid" variant="extended" aria-label="like">
                                <AddIcon />
                                Sukurti užsakymą
                            </Fab>
                        }
                        {
                            this.props.store!.isRepairShop &&
                            <NavLink to={"/ClientsBids"}>
                                <Fab id="createABid" variant="extended" aria-label="like">
                                    <AddIcon />
                                    Pateikti pasiūlymą
                            </Fab>
                            </NavLink>
                        }
                    </div>
                </div>
                <div className="HomeContainer" >
                    <div className="ContentCardTitle">
                        Mes faini nes
                    </div>
                    <div className="ContentCard">
                        <div className="CardContent" >
                            <div style={{ width: "50%" }}>
                                <div className="CardContentSectionName" >Mūsų klientai išsidėstę:</div>
                                <div className="mapouter">
                                    <div className="gmap_canvas">
                                        <iframe frameBorder="none" width="400" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" scrolling="no">
                                        </iframe>
                                    </div>
                                </div>
                            </div>
                            <div style={{ width: "50%" }}>
                                <div className="CardContentSectionName" >Mūsų vis daugėja:</div>
                                <Line data={data} />
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{ color: "rgba(0,0,0,0.5)", background: "white" }} className="ContentCardTitle">
                    Atsiliepimai
                </div>
                <div style={{ boxShadow: "none" }} className="ContentCard">
                    <div style={{ flexWrap: "wrap" }} className="CardContent" >
                        <ReviewCard showRating={true} />
                        <ReviewCard showRating={true} />
                    </div>
                </div>
            </div>
        );
    }
}
