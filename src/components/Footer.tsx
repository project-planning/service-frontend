import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import AppBar from '@material-ui/core/AppBar';
import Link from '@material-ui/core/Link';
import { NavLink } from 'react-router-dom';
import { inject, observer } from "mobx-react";
import '../App.css';
import { Typography } from '@material-ui/core';

@inject("store")
@observer
export default class Footer extends Component<IProviderProps> {
    render() {
        return (
            <div className="Footer">
                <div>
                    <div className="FooterSectionName" >(86) 000 0000</div>
                    <div className="SectionOptions">
                        <div>
                            Skambinkite nemokamai
                        </div>
                    </div>
                </div>
                <div>
                    <div className="FooterSectionName" >AutoAsas</div>
                    <div className="SectionOptions">
                        <div>
                            Ieškoti žemėlapyje
                        </div>
                        <div>
                            Kontaktai
                        </div>
                        <div>
                            Kaip tai veikia?
                        </div>
                    </div>
                </div>
                <div>
                    <div className="FooterSectionName" >Informacija</div>
                    <div className="SectionOptions">
                        <div>
                            Susitarimai
                        </div>
                        <div>
                            Pasiūlymai
                        </div>
                        <div>
                            Privatumas
                        </div>
                    </div>
                </div>
                <div>
                    <div className="FooterSectionName" >Apie projektą</div>
                    <div className="SectionOptions">
                        <div>
                           DUK
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}
