import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import avatar from '../pictures/avatar.png';
import { inject, observer } from "mobx-react";
import Rating from '@material-ui/lab/Rating';
import '../App.css';

export interface IHomeProps extends IProviderProps {
    showRating?: boolean;
}

@inject("store")
@observer
export default class Home extends Component<IHomeProps> {
    render() {
        return (
            <div className={"ReviewCard"}>
                <div>
                    <div style={{ backgroundImage: `url(${avatar})` }} className="Persona" >

                    </div>
                </div>
                <div className="ReviewCardMainInfo" >
                    <div className="SemiBold">User</div>
                    <div>Audi TT</div>
                    <div>
                        {
                            this.props.showRating &&
                            <Rating
                                style={{ marginLeft: "-5px" }}
                                value={3}
                                max={5}
                                onChange={(value: any) => console.log(`Rated with value ${value}`)}
                            />
                        }
                    </div>
                </div>
                <div>
                    <div className="SemiBold">Title</div>
                    <div>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Saepe perferendis expedita non omnis eveniet accusantium, tempore quisquam deserunt itaque accusamus, enim ipsam amet illo doloribus numquam voluptates at cumque hic?</div>
                </div>
                <div className="ReviewDate" >2019-11-17</div>
            </div>
        );
    }
}
