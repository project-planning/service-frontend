import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import { inject, observer } from "mobx-react";
import PropTypes from 'prop-types';
import '../App.css';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import { NavLink } from 'react-router-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';


export interface IUserLoginFormProps extends IProviderProps {
    setRegistered: (registered: boolean) => void;
    onLoginDataChange: (ev: any) => void;
    login: (isRepairShop: boolean) => void;
}

@inject("store")
@observer
export default class UserLoginForm extends Component<IUserLoginFormProps> {

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    {/* <Avatar>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Prisijungti
                    </Typography> */}
                    <form
                        //className={classes.form} 
                        noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="El. paštas"
                            name="login"
                            onChange={this.props.onLoginDataChange}
                            autoComplete="email"
                            autoFocus
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Slaptažodis"
                            type="password"
                            id="password"
                            onChange={this.props.onLoginDataChange}
                            autoComplete="current-password"
                        />
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Įsiminti mane"
                        />
                        <Button
                            //type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            onClick={() => this.props.login(false)}
                            className={"loginSubmit"}
                        >
                            Prisijungti
                                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link style={{ color: "#259b9a" }} href="#" variant="body2">
                                    Pamiršai slaptažodį?
                                </Link>
                            </Grid>
                            <Grid item>
                                <NavLink to="/">
                                    <Link onClick={() => this.props.setRegistered(false)} style={{ color: "#259b9a" }} href="#" variant="body2">
                                        {"Esi naujas vartotojas? Registruokis"}
                                    </Link>
                                </NavLink>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}
