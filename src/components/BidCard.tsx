import React, { Component } from 'react';
import { IProviderProps, IIssue, IOffer } from '../AutoAsasProps';
import avatar from '../pictures/avatar.png';
import { inject, observer } from "mobx-react";
import { observable, action } from 'mobx';
import MonetizationOn from '@material-ui/icons/MonetizationOn';
import Rating from '@material-ui/lab/Rating';
import '../App.css';
import { Grid, TextField, InputLabel, Button } from '@material-ui/core';

export interface IBidCardProps extends IProviderProps {
    showRating?: boolean;
    bid: IIssue;
}

@inject("store")
@observer
export default class BidCard extends Component<IBidCardProps> {

    @observable private expanded = false;
    @observable private offer: any = { issueId: this.props.bid.id, price: 0, description: "" };

    @observable private priceData: any = { workPrice: 0, workDiscount: 0, partsPrice: 0, partsDiscount: 0 };

    @action.bound
    private onChange(ev: any) {
        let value = ev.target.value;
        let name = ev.target.name;
        if (name === "description") {
            this.offer[name] = value;
        } else {
            value = Number(value);
            this.priceData[name] = value;
            let { workPrice, workDiscount, partsPrice, partsDiscount } = this.priceData;
            let price = workPrice * (1 - 0.01 * workDiscount) + partsPrice * (1 - 0.01 * partsDiscount);
            this.offer.price = price;
        }
    }

    @action.bound
    private createOffer() {
        this.props.store!.createOffer(this.offer);
    }

    @action.bound
    private toggleBidCard() {
        this.expanded = !this.expanded;
    }

    render() {
        let bid = this.props.bid;
        console.log(bid.offers);
        return (
            <div className={"BidCard"} >
                <div style={{ padding: "10px", width: "100%", boxSizing: "border-box", minHeight: "120px" }} >
                    <div>
                        <div style={{ backgroundImage: `url(${avatar})` }} className="Persona" >

                        </div>
                    </div>
                    <div onClick={this.toggleBidCard} className="ReviewCardMainInfo" >
                        <div className="SemiBold">Test</div>
                        <div><strong style={{ opacity: "0.5" }} >{bid.manufacturer.name + " " + bid.model.modelName + ", " + bid.productionYear}</strong></div>
                        <div style={{ opacity: 0.5 }} ><MonetizationOn style={{ marginRight: "5px" }} /> {bid.desiredPrice}</div>
                    </div>
                    <div style={{ width: "100%" }} onClick={this.toggleBidCard} >
                        <div>
                            <div className="SemiBold">Aprašymas</div>
                            <div>
                                {bid.description.split('\n').map((line) => {
                                    return <div>{line}</div>
                                })}
                            </div>
                        </div>
                        <div className="ReviewDate" >{bid.created.split('T')[0]}</div>
                    </div>
                    {
                        bid.offers.length != 0 &&
                        <div className={"NumberOfOffers"} ><span>{bid.offers.length}</span></div>
                    }
                </div>
                {(bid.offers.length > 0) ?
                    bid.offers.length != 0 &&
                    <div className={"RepairShopOffers " + ((this.expanded) ? "RepairShopActionsExpanded" : "")}>
                        <div className={"RepairOfferContainer"} >
                            {
                                bid.offers.map((offer: IOffer) => {
                                    return (
                                        <div style={{ position: "relative" }} key={offer.id} className={"RepairOffer"}>
                                            <div style={{borderBottom:"solid 1px rgba(0,0,0,0.2)", paddingBottom:"5px", marginBottom:"10px", color:"#259b9a"}} ><strong style={{ fontSize: "18px", opacity: 0.7 }}>{offer.autoService.name}</strong></div>
                                            <div><strong style={{ opacity: 0.7 }} >Kaina: </strong>{offer.price}€</div>
                                            <br />
                                            <div><strong style={{ opacity: 0.7 }}>Aprašymas</strong></div>
                                            <div>{offer.description}</div>
                                            <br />
                                            <div style={{ width: "100%", background: "rgba(0,0,0,0.05)", position: "absolute", bottom: 0, left: 0, padding: "10px", boxSizing:"border-box" }}><strong style={{ opacity: 0.7 }} >Telefonas: </strong>{offer.autoService.contactPhone}<strong style={{ opacity: 0.7 }} > El. paštas: </strong>{offer.autoService.contactEmail}</div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                    :
                    this.props.store!.isRepairShop &&
                    <div className={"RepairShopActions " + ((this.expanded) ? "RepairShopActionsExpanded" : "")} >

                        <div>
                            <InputLabel >Remonto darbai</InputLabel>
                            <TextField onChange={this.onChange} name="workPrice" placeholder={"Kaina €‎"} />
                        </div>
                        <div>
                            <TextField onChange={this.onChange} name="workDiscount" placeholder={"Nuolaida %"} />
                        </div>
                        <div>
                            <InputLabel >Automobilio dalys</InputLabel>
                            <TextField onChange={this.onChange} name="partsPrice" placeholder={"Kaina €‎"} />
                        </div>
                        <div>
                            <TextField onChange={this.onChange} name="partsDiscount" placeholder={"Nuolaida %"} />
                        </div>
                        <div>
                            <InputLabel >Suma</InputLabel>
                            <TextField value={this.offer.price} onChange={this.onChange} name="price" />
                        </div>
                        <div style={{ width: "100%" }} >
                            <TextField onChange={this.onChange} style={{ width: "100%" }} rows={5} multiline name="description" placeholder={"Parašykyte motyvacinį laišką"} />
                        </div>
                        <div style={{ width: "100%" }}>
                            <Button
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={"loginSubmit"}
                                style={{ width: "250px" }}
                                onClick={this.createOffer}
                            >
                                Pateikti pasiūlymą
                        </Button>
                        </div>

                    </div>
                }
            </div>
        );
    }
}
