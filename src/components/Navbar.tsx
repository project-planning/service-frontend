import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import AppBar from '@material-ui/core/AppBar';
import avatar from '../pictures/avatar.png';
import Icon from '@material-ui/core/Icon';
import MailOutline from '@material-ui/icons/MailOutline';
import Link from '@material-ui/core/Link';
import { NavLink } from 'react-router-dom';
import { inject, observer } from "mobx-react";
import '../App.css';
import { Typography } from '@material-ui/core';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import { observable, action } from 'mobx';
import LoginForm from './LoginDialog';

@inject("store")
@observer
export default class Navbar extends Component<IProviderProps> {

    @observable private growMenuOpen = false;

    @action.bound
    private handleClose() {
        this.props.store!.openLoginDialog = false;
    }

    @action.bound
    private handleOpen() {
        this.props.store!.openLoginDialog = true;
    }

    @action.bound
    private handleToggle() {
        this.growMenuOpen = !this.growMenuOpen;
    };

    @action.bound
    private handleGrowMenuClose() {
        this.growMenuOpen = false;
    };

    @action.bound
    private handleGrowMenuOpen() {
        this.growMenuOpen = true;
    };

    @action.bound
    private handleListKeyDown(event: React.KeyboardEvent<HTMLUListElement>) {
        if (event.key === 'Tab') {
            event.preventDefault();
            this.growMenuOpen = true;
        }
    }

    render() {
        let anchorRef: any = React.createRef();
        return (
            <div className="Navbar">
                <AppBar className="AppBarContainer" position="static">
                    <div className="NavBarItem"><NavLink to={"/"}><Typography>Titulinis</Typography></NavLink></div>
                    <div className="NavBarItem"><NavLink to={"/repairshopsmap"}><Typography>Ieškoti žemėlapyje</Typography></NavLink></div>
                    {
                        !this.props.store!.isRepairShop && this.props.store!.loggedIn &&
                        <div className="NavBarItem"><NavLink to={"/MyBids"}><Typography>Mano užsakymai</Typography></NavLink></div>
                    }
                    {
                        this.props.store!.isRepairShop && this.props.store!.loggedIn &&
                        <div className="NavBarItem"><NavLink to={"/ClientsBids"}><Typography>Klientų užsakymai</Typography></NavLink></div>
                    }
                    {
                        <div className="UserProfileContainer">
                            <div><MailOutline /></div>
                            <div ref={(ref) => { anchorRef = ref; }} onClick={this.props.store!.loggedIn ? this.handleToggle : this.handleOpen} style={{ backgroundImage: `url(${avatar})` }} className={"UserProfile"} ></div>
                        </div>
                    }
                </AppBar>
                {
                    this.props.store!.loggedIn &&
                    <Popper style={{ position: "absolute", right: "10px", left: "auto", top: "40px", zIndex: 2 }} open={this.growMenuOpen} anchorEl={(anchorRef != null) ? anchorRef.current : null} role={undefined} transition disablePortal>
                        {({ TransitionProps, placement }) => (
                            <Grow
                                {...TransitionProps}
                                style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                            >
                                <Paper>
                                    <ClickAwayListener onClickAway={this.handleClose}>
                                        <MenuList autoFocusItem={this.growMenuOpen} id="menu-list-grow" onKeyDown={this.handleListKeyDown}>
                                            <MenuItem onClick={this.handleGrowMenuClose}><NavLink style={{textDecoration:"none"}} to={"/"}>Profilis</NavLink></MenuItem>
                                            <MenuItem onClick={this.handleGrowMenuClose}><NavLink style={{textDecoration:"none"}} to={"/"}>Mano paskyra</NavLink></MenuItem>
                                            <MenuItem onClick={this.props.store!.logout}><NavLink style={{textDecoration:"none"}} to={"/"}>Atsijungti</NavLink></MenuItem>
                                        </MenuList>
                                    </ClickAwayListener>
                                </Paper>
                            </Grow>
                        )}
                    </Popper>
                }
                <LoginForm handleOpen={this.handleOpen} handleClose={this.handleClose} open={this.props.store!.openLoginDialog} />
            </div >
        );
    }
}
