import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import { inject, observer } from "mobx-react";
import PropTypes from 'prop-types';
import '../App.css';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';


export interface IUserRegistrationFormProps extends IProviderProps {
    setRegistered: (registered: boolean) => void;
}

@inject("store")
@observer
export default class UserRegistrationForm extends Component<IUserRegistrationFormProps> {

    render() {
        return (
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div style={{
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                }}>
                    {/* <Avatar>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography style={{ paddingBottom: "10px" }} component="h1" variant="h5">
                        Registracija
                    </Typography> */}
                    <form noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="fname"
                                    name="firstName"
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="firstName"
                                    label="Vardas"
                                    autoFocus
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="lastName"
                                    label="Pavardė"
                                    name="lastName"
                                    autoComplete="lname"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="email"
                                    label="El. paštas"
                                    name="email"
                                    autoComplete="email"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    id="phoneNumber"
                                    label="Telefono numeris"
                                    name="phoneNumber"
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    variant="outlined"
                                    required
                                    fullWidth
                                    name="password"
                                    label="Slaptažodis"
                                    type="password"
                                    id="password"
                                    autoComplete="current-password"
                                />
                            </Grid>
                            <Grid item xs={12}>
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={"loginSubmit"}
                        >
                            Registruotis
                        </Button>
                        <Grid container justify="flex-end">
                            <Grid item>
                                <Link style={{ color: "#259b9a" }} onClick={() => this.props.setRegistered(true)} href="#" variant="body2">
                                    Jau turi paskyrą? Prisijunk
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
        );
    }
}
