import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import { inject, observer } from "mobx-react";
import PropTypes from 'prop-types';
import '../App.css';
import { Typography } from '@material-ui/core';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Box from '@material-ui/core/Box';
import { observable, action, toJS } from 'mobx';
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';

export interface IBidCreationProps extends IProviderProps {
    handleClose: () => void;
    handleOpen: () => void;
    open: boolean;
}

function TabPanel(props: any) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

@inject("store")
@observer
export default class BidCreationForm extends Component<IBidCreationProps> {

    @observable private selectedTab = 0;
    @observable private registered = true;
    @observable private bid: any = {
        manufacturerId: 0,
        modelId: 0,
        productionYear: 0,
        description: "",
        vin: "new",
        desiredPrice: 0
    }

    public componentDidMount() {
        this.props.store!.getManufacturers();
    }

    @action.bound
    private setRegistered(registered: boolean) {
        this.registered = registered;
    }

    @action.bound
    private handleChange(ev: React.ChangeEvent<{}>, value: number) {
        this.selectedTab = value;
    }

    private a11yProps(index: number) {
        return {
            id: `scrollable-auto-tab-${index}`,
            'aria-controls': `scrollable-auto-tabpanel-${index}`,
        };
    }

    @action.bound
    private onChange(ev: any) {
        let value = ev.target.value;
        let name = ev.target.name;
        if(name == "productionYear" || name == "desiredPrice") {
            value = Number(value);
        }
        this.bid[name] = value;
        if (name === "manufacturerId" && value != null && value != "") {
            this.props.store!.getModels(value);
        }
    }

    @action.bound
    private createBid() {
        this.props.store!.createIssue(this.bid);
        this.props.handleClose();
    }

    render() {
        return (
            <div className="Login">
                <Dialog
                    open={this.props.open}
                    onClose={this.props.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle>{"Sukurti užsakymą"}</DialogTitle>
                    <DialogContent>
                        <div className="BidCreationForm" >
                            <FormControl>
                                <Grid container spacing={2}>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl>
                                            <InputLabel id="manufacturer">Markė</InputLabel>
                                            <Select
                                                labelId="manufacturer"
                                                name="manufacturerId"
                                                title={"Markė"}
                                                placeholder={"Markė"}
                                                // value={age}
                                                //onChange={handleChange}
                                                onChange={this.onChange}
                                                displayEmpty
                                                className={"BidCreationFromField"}
                                            >
                                                {
                                                    this.props.store!.manufacturers.map((manufacturer: any) => {
                                                        return <MenuItem value={manufacturer.id}>{manufacturer.name}</MenuItem>
                                                    })
                                                }
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl>
                                            <InputLabel id="model">Modelis</InputLabel>
                                            <Select
                                                labelId="model"
                                                name="modelId"
                                                id="demo-simple-select"
                                                title={"Modelis"}
                                                placeholder={"Modelis"}
                                                onChange={this.onChange}
                                                disabled={this.bid.manufacturerId == null}
                                                // value={age}
                                                //onChange={handleChange}
                                                displayEmpty
                                                className={"BidCreationFromField"}
                                            >
                                                {
                                                    this.props.store!.models.map((model: any) => {
                                                        return <MenuItem value={model.id}>{model.modelName}</MenuItem>
                                                    })
                                                }
                                            </Select>
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl>
                                            <TextField
                                                // value={age}
                                                //onChange={handleChange}
                                                name={"productionYear"}
                                                onChange={this.onChange}
                                                label={"Pagaminimo metai"}
                                                inputProps={{ min: "1990", max: "2020", step: "1" }}
                                                type="number"
                                                className={"BidCreationFromField"}
                                            />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12} sm={6}>
                                        <FormControl>
                                            <TextField
                                                // value={age}
                                                //onChange={handleChange}
                                                name={"desiredPrice"}
                                                onChange={this.onChange}
                                                label={"Pageidaujama suma"}
                                                inputProps={{ min: "0", step: "1" }}
                                                type="number"
                                                className={"BidCreationFromField"}
                                            />
                                        </FormControl>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <TextField name={"description"} onChange={this.onChange} rows={5} multiline label={"Problemos aprašymas"} />
                                    </Grid>
                                </Grid>
                            </FormControl>
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={"loginSubmit"}
                            onClick={this.createBid}
                        >
                            Sukurti
                        </Button>
                    </DialogActions>
                </Dialog>
            </div >
        );
    }
}
