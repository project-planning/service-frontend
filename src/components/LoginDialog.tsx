import React, { Component } from 'react';
import { IProviderProps } from '../AutoAsasProps';
import { inject, observer } from "mobx-react";
import PropTypes from 'prop-types';
import '../App.css';
import { Typography } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContentText from '@material-ui/core/DialogContentText';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import { observable, action } from 'mobx';
import UserLoginForm from './UserLoginForm';
import RapairShopLoginForm from './RepairShopLoginForm';
import UserRegistrationForm from './UserRegistrationForm';
import RepairShopRegistrationForm from './RepairShopRegistrationForm';
import { ILoginBody } from '../AutoAsasProps';


export interface ILoginFormProps extends IProviderProps {
    handleClose: () => void;
    handleOpen: () => void;
    open: boolean;
}

function TabPanel(props: any) {
    const { children, value, index, ...other } = props;

    return (
        <Typography
            component="div"
            role="tabpanel"
            hidden={value !== index}
            id={`scrollable-auto-tabpanel-${index}`}
            aria-labelledby={`scrollable-auto-tab-${index}`}
            {...other}
        >
            <Box p={3}>{children}</Box>
        </Typography>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

@inject("store")
@observer
export default class LoginForm extends Component<ILoginFormProps> {

    @observable private selectedTab = 0;
    @observable private registered = true;
    @observable private loginData: ILoginBody = { password: "", login: "" };

    @action.bound
    private setRegistered(registered: boolean) {
        this.registered = registered;
    }

    @action.bound
    private onLoginDataChange(ev: any) {
        let name: "password" | "login" = ev.target.name;
        let value: string = ev.target.value;
        this.loginData[name] = value;
    }

    @action.bound
    private login(isRepairShop: boolean) {
        this.props.store!.login(this.loginData);
        // this.props.store!.isRepairShop = isRepairShop;
    }

    @action.bound
    private handleChange(ev: React.ChangeEvent<{}>, value: number) {
        this.selectedTab = value;
    }

    private a11yProps(index: number) {
        return {
            id: `scrollable-auto-tab-${index}`,
            'aria-controls': `scrollable-auto-tabpanel-${index}`,
        };
    }

    render() {
        return (
            <div className="Login">
                <Dialog
                    open={this.props.open && !this.props.store!.loggedIn}
                    onClose={this.props.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle>{"Kliento " + (this.registered ? "prisijungimas" : "registracija")}</DialogTitle>
                    {
                        this.registered ?
                            <DialogContent>
                                <UserLoginForm login={this.login} onLoginDataChange={this.onLoginDataChange} setRegistered={this.setRegistered} />
                            </DialogContent>
                            :
                            <DialogContent>
                                <Tabs
                                    value={this.selectedTab}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    onChange={this.handleChange}
                                    aria-label="disabled tabs example"
                                >
                                    <Tab label="Vartotojas" {...this.a11yProps(0)} />
                                    <Tab label="Autoservisas" {...this.a11yProps(1)} />
                                </Tabs>
                                <TabPanel value={this.selectedTab} index={0}>
                                    {
                                        <UserRegistrationForm setRegistered={this.setRegistered} />
                                    }
                                </TabPanel>
                                <TabPanel value={this.selectedTab} index={1}>
                                    {
                                        <RepairShopRegistrationForm setRegistered={this.setRegistered} />
                                    }
                                </TabPanel>
                            </DialogContent>
                    }
                    <DialogActions>
                    </DialogActions>
                </Dialog>
            </div >
        );
    }
}
