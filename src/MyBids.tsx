import React, { Component } from 'react';
import { IProviderProps } from './AutoAsasProps';
import homeHeader from './pictures/homeHeader.jpg';
import { observable, action } from 'mobx';
import avatar from './pictures/avatar.png';
import { Fab, Tabs, Tab } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { inject, observer } from "mobx-react";
import BidCard from './components/BidCard';
import BidCreationForm from './components/BidCreationDialog';

@inject("store")
@observer
export default class MyBids extends Component<IProviderProps> {

    @observable private open = false;

    @action.bound
    private handleClose() {
        this.open = false;
    }

    @action.bound
    private handleOpen() {
        this.open = true;
    }

    public componentWillMount() {
        const urlParams = new URLSearchParams(window.location.search);
        const dialog = urlParams.get('dialog');
        this.open = dialog === 'true';
        this.props.store!.getMyIssues();
        this.props.store!.getManufacturers();
    }

    render() {
        return (
            <div>
                <div className="MyBidsHeader" >
                    <div id="headerImage" style={{ backgroundImage: `url(${homeHeader})` }}></div>
                    <div id="headerCover" >
                        <div className="AutoAsasBest" >
                            <div id="title" >Mano užsakymai</div>
                        </div>
                        <Fab onClick={this.handleOpen} id="createABid" variant="extended" aria-label="like">
                            <AddIcon />
                            Sukurti užsakymą
                        </Fab>
                    </div>
                </div>
                <div style={{ boxShadow: "none", padding: "0 0 10px 0", backgroundColor: "#f5f5f5", paddingTop:"20px" }} className="ContentCard">
                    {/* <Tabs
                        value={1}
                        // onChange={handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        style={{ backgroundColor: "white", marginBottom: "15px" }}
                    >
                        <Tab label="Naujausi" />
                        <Tab label="Populiariausi" />
                    </Tabs> */}
                    <BidCreationForm handleOpen={this.handleOpen} handleClose={this.handleClose} open={this.open} />
                    <div style={{ flexWrap: "wrap" }} className="CardContent" >
                        {
                            this.props.store!.myIssues.map((bid) => {
                                return <BidCard bid={bid} />;
                            })
                        }
                    </div>
                </div>
            </div>
        );
    }
}
