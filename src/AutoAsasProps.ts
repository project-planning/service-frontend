import { MainStore } from './MainStore';

export interface IProviderProps {
    store?: MainStore;
}

export interface ILoginBody {
    login: string;
    password: string;
}

export interface IManufacturer {
    id: String;
    name: String
}

export interface IModel {
    id: number;
    manufacturerId: number;
    modelName: String;
    additionalInfo: String;
    doorCount: number;
    productionStartYear: number;
    productionEndYear: number;
    transmissionType: String;
    fuelType: String;
    pictureUrl: String
}

export interface IRepairShop {
    "id": 0,
    "name": "string",
    "description": "string",
    "address": "string",
    "contactEmail": "string",
    "contactPhone": "string"
}

export interface IOffer {
    id: number;
    issueId: number;
    price: number;
    description: String;
    autoService: IRepairShop
}

export interface IIssue {
    id: number;
    manufacturer: IManufacturer;
    model: IModel;
    productionYear: number;
    description: String;
    vin: String;
    clientId: number;
    created: String;
    desiredPrice: number;
    selectedOfferId: number;
    offers: IOffer[];
}