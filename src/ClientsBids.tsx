import React, { Component } from 'react';
import { IProviderProps } from './AutoAsasProps';
import homeHeader from './pictures/homeHeader.jpg';
import { observable, action } from 'mobx';
import avatar from './pictures/avatar.png';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { inject, observer } from "mobx-react";
import ReviewCard from './components/ReviewCard';
import BidCreationForm from './components/BidCreationDialog';
import BidCard from './components/BidCard';
import { Paper, Tabs, Tab } from '@material-ui/core';

@inject("store")
@observer
export default class ClientsBids extends Component<IProviderProps> {

    @observable private open = false;
    @observable private selectedTabIndex = 1;

    public componentDidMount() {
        this.props.store!.getMyOffers().then(() => {
            this.props.store!.getAllIssues();
        });
    }

    @action.bound
    private handleClose() {
        this.open = false;
    }

    @action.bound
    private handleOpen() {
        this.open = true;
    }

    private handleChange = (event: any, newValue: any) => {
        this.selectedTabIndex = newValue;
    };

    render() {
        console.log(console.log(this.props.store!.myIssues));
        return (
            <div>
                <div className="MyBidsHeader" >
                    <div id="headerImage" style={{ backgroundImage: `url(${homeHeader})` }}></div>
                    <div id="headerCover" >
                        <div className="AutoAsasBest" >
                            <div id="title" >Klientų užsakymai</div>
                        </div>
                    </div>
                </div>
                <div style={{ boxShadow: "none", padding: "0 0 10px 0", backgroundColor: "#f5f5f5" }} className="ContentCard">
                    <Tabs
                        value={this.selectedTabIndex}
                        onChange={this.handleChange}
                        indicatorColor="primary"
                        textColor="primary"
                        style={{ backgroundColor: "white", marginBottom: "15px" }}
                    >
                        <Tab id={"0"} label="Visi klientai" ></Tab>
                        <Tab id={"1"} label="Mano klientai" ></Tab>
                    </Tabs>
                    <BidCreationForm handleOpen={this.handleOpen} handleClose={this.handleClose} open={this.open} />
                    {
                        this.selectedTabIndex == 1 &&
                        <div style={{ flexWrap: "wrap" }} className="CardContent" >
                            {
                                this.props.store!.myIssues.map((bid) => {
                                    return <BidCard key={bid.id} bid={bid} />;
                                })
                            }
                        </div>
                    }
                    {
                        this.selectedTabIndex == 0 &&
                        <div style={{ flexWrap: "wrap" }} className="CardContent" >
                            {
                                this.props.store!.allIssues.map((bid) => {
                                    return <BidCard key={bid.id} bid={bid} />;
                                })
                            }
                        </div>
                    }
                </div>
            </div>
        );
    }
}
