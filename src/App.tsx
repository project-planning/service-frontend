import React, { Component } from 'react';
import { Route, Router, BrowserRouter, Switch, HashRouter, StaticRouter } from 'react-router-dom';
import { inject, observer } from "mobx-react";
import Home from './Home';
import MyBids from './MyBids';
import ClientsBids from './ClientsBids';
import RepairShopsMap from './RepairShopsMap';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import './App.css';
import { IProviderProps } from './AutoAsasProps';

@inject("store")
@observer
export class App extends Component<IProviderProps>{

  public componentDidMount() {
    this.props.store!.checkJWTExpiration();
  }

  render() {
    return (
      <BrowserRouter>
        <div>
          <Navbar />
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/MyBids" component={MyBids} />
            <Route exact path="/ClientsBids" component={ClientsBids} />
            <Route exact path="/repairshopsmap" component={RepairShopsMap} />
          </Switch>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}
