import React, { Component } from 'react';
import { IProviderProps } from './AutoAsasProps';
import map from './pictures/map.jpg';
import { observable, action } from 'mobx';
import avatar from './pictures/avatar.png';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import { inject, observer } from "mobx-react";
import ReviewCard from './components/ReviewCard';
import BidCreationForm from './components/BidCreationDialog';
import BidCard from './components/BidCard';
import { Paper, Tabs, Tab } from '@material-ui/core';

@inject("store")
@observer
export default class RepairShopsMap extends Component<IProviderProps> {

    @observable private open = false;

    @action.bound
    private handleClose() {
        this.open = false;
    }

    @action.bound
    private handleOpen() {
        this.open = true;
    }

    render() {
        return (
            <div>
                <div className="MyBidsHeader" >
                    <div id="headerImage" style={{ backgroundImage: `url(${map})` }}></div>
                    <div id="headerCover" >
                        <div className="AutoAsasBest" >
                            <div id="title" >Autoservisų žemėlapis</div>
                        </div>
                    </div>
                </div>
                <div style={{ boxShadow: "none", padding: "0 0 10px 0", backgroundColor: "#f5f5f5" }} className="ContentCard">
                    <div className="mapouter">
                        <div className="gmap_canvas">
                            <iframe frameBorder="none" width="400" height="800" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" scrolling="no">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
