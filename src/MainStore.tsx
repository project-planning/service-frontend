import { observable, action } from 'mobx';
import { ILoginBody, IManufacturer, IIssue, IOffer } from './AutoAsasProps';
import jwtDecode from 'jwt-decode';

export class MainStore {
    @observable public loggedIn = false;
    @observable public isRepairShop = false;
    @observable public openLoginDialog = false;
    @observable public manufacturers: IManufacturer[] = [];
    @observable public models = [];
    @observable public myIssues: IIssue[] = [];
    @observable public allIssues: IIssue[] = [];
    @observable public myOffers: IOffer[] = [];

    @action.bound
    public login(body: ILoginBody) {
        return fetch('http://54.93.86.14/api/auth/login', { method: 'POST', headers: { "Content-type": "application/json" }, body: JSON.stringify(body) }).then((response) => {
            if (response.ok != false) {
                response.json().then((res) => {
                    sessionStorage.setItem("abraKadabra", res.accessToken);
                    this.loggedIn = true;
                    this.checkJWTExpiration();
                });
            } else {
                return [];
            }
        })
    }

    public checkJWTExpiration() {
        let jwt: any = sessionStorage.getItem("abraKadabra");
        if (jwt != null) {
            jwt = jwtDecode(jwt);
            const current_time = new Date().getTime() / 1000;
            if (current_time > jwt.exp) {
                sessionStorage.removeItem("abraKadabra");
                this.loggedIn = false;
                window.location.href = '/';
            } else {
                this.loggedIn = true;
                if (jwt.role === "RepairShop" || (typeof jwt.role != "string" && jwt.role.filter((role: string) => { return role === "RepairShop"; }).length > 0)) {
                    this.isRepairShop = true;
                } else {
                    this.isRepairShop = false;
                }
            }
        }
    }

    @action.bound
    public getAllIssues() {
        const jwtToken: any = sessionStorage.getItem("abraKadabra");
        return fetch('http://54.93.86.14/api/issues/', { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken, } }).then((response) => {
            if (response.ok != false) {
                response.json().then((res) => {
                    res.forEach((issue: IIssue) => {
                        issue.offers = this.myOffers.filter((offer) => { return offer.issueId == issue.id; });
                    })
                    this.allIssues = res;
                });
            } else {
                return [];
            }
        })
    }

    @action
    public getMyIssues() {
        const jwtToken: any = sessionStorage.getItem("abraKadabra");
        return fetch('http://54.93.86.14/api/issues/my', { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken, } }).then((response:any) => {
            if (response.ok != false) {
                this.myIssues = [];
                return response.json().then(async (res:any) => {
                    let temp: IIssue[] = [];
                    res.forEach(async (issue: IIssue) => {
                        issue.offers = [];
                        await this.getOffersByIssueId(issue.id).then((offers) => {
                            issue.offers = offers;
                            this.myIssues.push(issue);
                        })
                    })
                });
            } else {
                return [];
            }
        })
    }

    @action.bound
    public getMyOffers() {
        const jwtToken: any = sessionStorage.getItem("abraKadabra");
        return fetch('http://54.93.86.14/api/offers/my', { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken, } }).then((response) => {
            if (response.ok != false) {
                response.json().then((res) => {
                    this.myOffers = res;
                    res.forEach(async (offer: IOffer) => {
                        await this.getIssueById(offer.issueId).then((issue: IIssue) => {
                            issue.offers = [];
                            issue.offers.push(offer);
                            this.myIssues.push(issue);
                        });
                    })
                });
            } else {
                return [];
            }
        })
    }

    @action.bound
    public createIssue(newIssue: any) {
        console.log(newIssue);
        const jwtToken: any = sessionStorage.getItem("abraKadabra");
        return fetch('http://54.93.86.14/api/issues/', { method: 'POST', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken, "Content-type": "application/json" }, body: JSON.stringify(newIssue) }).then((response) => {
            this.getMyIssues();
        })
    }

    @action.bound
    public createOffer(newOffer: any) {
        const jwtToken: any = sessionStorage.getItem("abraKadabra");
        return fetch(`http://54.93.86.14/api/offers/api/issues/${newOffer.issueId}/offers`, { method: 'POST', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken, "Content-type": "application/json" }, body: JSON.stringify(newOffer) }).then((response) => {
            if (response.ok != false) {
                this.getMyOffers();
                this.getAllIssues();
                response.json().then((res) => {
                    console.log(res);
                });
            } else {
                return [];
            }
        })
    }

    @action.bound
    public getOffersByIssueId(Id: number) {
        const jwtToken = sessionStorage.getItem("abraKadabra");
        return fetch(`http://54.93.86.14/api/offers/api/issues/${Id}/offers`, { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken } }).then((response) => {
            if (response.ok != false) {
                return response.json();
            } else {
                return [];
            }
        })
    }

    @action.bound
    public getIssueById(Id: number) {
        const jwtToken = sessionStorage.getItem("abraKadabra");
        return fetch(`http://54.93.86.14/api/issues/${Id}`, { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken } }).then((response) => {
            if (response.ok != false) {
                return response.json();
            } else {
                return [];
            }
        })
    }

    @action.bound
    public getManufacturers() {
        const jwtToken = sessionStorage.getItem("abraKadabra");
        console.log(jwtToken)

        return fetch('http://54.93.86.14/api/manufacturers', { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken } }).then((response) => {
            if (response.ok != false) {
                response.json().then((res) => {
                    this.manufacturers = res.manufacturers;
                });
            } else {
                return [];
            }
        })
    }

    @action.bound
    public getModels(manufacturerId: string) {
        const jwtToken = sessionStorage.getItem("abraKadabra");
        return fetch(`http://54.93.86.14/api/manufacturers/${manufacturerId}/models`, { method: 'GET', headers: { "accept": "application/json", "Authorization": "Bearer " + jwtToken } }).then((response) => {
            if (response.ok != false) {
                response.json().then((res) => {
                    this.models = res.availableCarModels;
                });
            } else {
                return [];
            }
        })
    }

    @action.bound
    public logout() {
        sessionStorage.removeItem("abraKadabra");
        this.loggedIn = false;
    }

}